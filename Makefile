# set default shell
SHELL := $(shell which sh)
ENV = /usr/bin/env
# default shell options
.SHELLFLAGS = -e -c

CHART_DIR = charts
DIST_DIR = public
REPO_URL = https://csanquer.gitlab.io/
REPO_NAME = csanquer
REPO_DIR = helm-public-packages


opt =

.SILENT: ;               # no need for @
.ONESHELL: ;             # recipes execute in same shell
.NOTPARALLEL: ;          # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell

########################################################################
default: help

help: ## display help for make commands
	grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help

all: ## all main commands
	$(MAKE) check
	$(MAKE) build
.PHONY: all


# ########################################################################

.PHONY: check
check: ## check and lint all charts
	for d in $$(find $(CHART_DIR) -mindepth 1 -maxdepth 1 -type d ); do \
		echo $$(basename $$d); \
		helm lint $$d ; \
	done ;

.PHONY: build
build: ## build all charts
	for d in $$(find $(CHART_DIR) -mindepth 1 -maxdepth 1 -type d ); do \
		echo $$(basename $$d) ;\
		helm package $$d -d $(DIST_DIR) ;\
	done ;\

.PHONY: index
index: ## generate local helm repository index
	helm repo index $(DIST_DIR) --url $(REPO_URL)/$(REPO_DIR)/

.PHONY: add_repo
add_repo: ## add helm repository
	helm repo add $(REPO_NAME) $(REPO_URL)/$(REPO_DIR)/

.PHONY: deploy
deploy: ## deploy all charts
	$(MAKE) build
	$(MAKE) index
